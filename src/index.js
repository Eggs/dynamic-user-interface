import { viewController } from "./viewController";
import { imageSliderViewController } from "./imageSliderViewController";

const main = () => {
  const view = viewController();

  const addDropDownEvent = () => {
    const dropdown = document.querySelectorAll(".dropdown");
    const dropdownData = document.querySelectorAll(".dropdown-data");

    dropdown.forEach((element) => {
      element.addEventListener("click", (e) => {
        view.updateStyle(e.target.nextElementSibling);
      });
    });

    dropdownData.forEach((dropdownElement) => {
      dropdownElement.addEventListener("mouseleave", (e) => {
        if (!dropdownElement.classList.contains("visible")) {
          view.updateStyle(e.target);
        }
      });
    });
  };

  return {
    addDropDownEvent,
  };
};

//export { main };

const app = main();
app.addDropDownEvent();

const imageSliderView = imageSliderViewController();

// Initial setup of image slider.
imageSliderView.setImage(0);
imageSliderView.addNavigationEventListener();
imageSliderView.createNavigationDots();
imageSliderView.addNavigationDotEventListener();

imageSliderView.advanceWithTimer(5000);
