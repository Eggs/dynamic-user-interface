const imageSliderModelController = () => {
  const images = [
    "http://placekitten.com/200/300",
    "http://placekitten.com/300/300",
    "http://placekitten.com/200/200",
    "http://placekitten.com/300/300",
  ];

  return {
    images,
  };
};

export { imageSliderModelController };
