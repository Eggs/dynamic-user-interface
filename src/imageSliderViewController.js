import { imageSliderModelController } from "./imageSliderModelController";

const imageSliderViewController = () => {
  const imageSliderModel = imageSliderModelController();

  const imageSlider = document.querySelector("#image-slider");
  const imageSliderContainer = document.querySelector(
    "#image-slider-navigation-container"
  );
  const navRight = document.querySelector("#image-slider-navigation-right");

  let sliderCount = 0;
  const numOfImages = imageSliderModel.images.length - 1;

  const setDotActive = (num) => {
    const dot = document.querySelector(`#navigation-dot-${num}`);
    dot.classList.toggle("active");
  };

  const setImage = (num) => {
    imageSlider.style.background =
      "url('" + imageSliderModel.images[num] + "') no-repeat center";
  };

  const setAllDotsNotActive = () => {
    const dots = document.querySelectorAll(`.dot`);
    console.log({ dots });

    dots.forEach((dot) => {
      if (dot.classList.contains("active")) {
        console.log(dot);
        dot.classList.toggle("active");
      }
    });
  };

  const createNavigationDots = () => {
    let i = 0;
    while (i <= numOfImages) {
      const navigationDot = document.createElement("div");
      navigationDot.setAttribute("id", `navigation-dot-${i}`);
      if (i === 0) {
        navigationDot.classList.toggle("dot");
        navigationDot.classList.toggle("active");
      } else {
        navigationDot.classList.toggle("dot");
      }

      imageSliderContainer.insertBefore(navigationDot, navRight);

      i++;
    }
  };

  const addNavigationDotEventListener = () => {
    const dots = document.querySelectorAll(`.dot`);

    dots.forEach((dot) => {
      dot.addEventListener("click", (e) => {
        const dotId = e.target.getAttribute("id");
        setAllDotsNotActive();
        sliderCount = dotId[dotId.length - 1];
        setImage(sliderCount);
        setDotActive(sliderCount);
      });
    });
  };

  const addNavigationEventListener = () => {
    const navLeft = document.querySelector("#image-slider-navigation-left");
    const navRight = document.querySelector("#image-slider-navigation-right");
    const navigation = [navLeft, navRight];

    navigation.forEach((nav) => {
      nav.addEventListener("click", (e) => {
        if (e.target.getAttribute("id") === "image-slider-navigation-left") {
          if (sliderCount >= 1) {
            sliderCount--;
            setImage(sliderCount);
            setAllDotsNotActive();
            setDotActive(sliderCount);
          }
        } else {
          if (sliderCount < imageSliderModel.images.length - 1) {
            sliderCount++;
            setImage(sliderCount);
            setAllDotsNotActive();
            setDotActive(sliderCount);
          }
        }
        console.log(sliderCount);
      });
    });
  };

  const advanceWithTimer = (miliseconds) => {
    // let i = 0;
    window.setInterval(() => {
      setImage(sliderCount);
      setAllDotsNotActive();
      if (sliderCount >= numOfImages + 1) {
        setAllDotsNotActive();
        sliderCount = 0;
        setImage(sliderCount);
        setDotActive(sliderCount);
      }

      if (sliderCount !== 0) {
        setAllDotsNotActive();
        setDotActive(sliderCount);
      }

      sliderCount++;
    }, miliseconds);
  };

  return {
    setImage,
    createNavigationDots,
    addNavigationEventListener,
    addNavigationDotEventListener,
    advanceWithTimer,
  };
};

export { imageSliderViewController };
