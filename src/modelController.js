const modelController = () => {
  const checkSiblingElement = (element) => {
    if (element.classList.contains("dropdown-data")) {
      return true;
    } else {
      return false;
    }
  };

  return {
    checkSiblingElement,
  };
};

export { modelController };
