import { modelController } from "./modelController";

const viewController = () => {
  const model = modelController();

  const updateStyle = (element) => {
    if (model.checkSiblingElement(element)) {
      element.classList.toggle("visible");
      if (window.innerWidth > 600) {
        element.style.left = element.previousElementSibling.offsetLeft + "px";
      } else {
        element.style.left = "auto";
      }
    }
  };

  return {
    updateStyle,
  };
};

export { viewController };
