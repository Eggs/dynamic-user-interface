/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "dist";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/imageSliderModelController.js":
/*!*******************************************!*\
  !*** ./src/imageSliderModelController.js ***!
  \*******************************************/
/*! exports provided: imageSliderModelController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "imageSliderModelController", function() { return imageSliderModelController; });
var imageSliderModelController = function imageSliderModelController() {
  var images = ["http://placekitten.com/200/300", "http://placekitten.com/300/300", "http://placekitten.com/200/200", "http://placekitten.com/300/300"];
  return {
    images: images
  };
};



/***/ }),

/***/ "./src/imageSliderViewController.js":
/*!******************************************!*\
  !*** ./src/imageSliderViewController.js ***!
  \******************************************/
/*! exports provided: imageSliderViewController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "imageSliderViewController", function() { return imageSliderViewController; });
/* harmony import */ var _imageSliderModelController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./imageSliderModelController */ "./src/imageSliderModelController.js");


var imageSliderViewController = function imageSliderViewController() {
  var imageSliderModel = Object(_imageSliderModelController__WEBPACK_IMPORTED_MODULE_0__["imageSliderModelController"])();
  var imageSlider = document.querySelector("#image-slider");
  var imageSliderContainer = document.querySelector("#image-slider-navigation-container");
  var navRight = document.querySelector("#image-slider-navigation-right");
  var sliderCount = 0;
  var numOfImages = imageSliderModel.images.length - 1;

  var setDotActive = function setDotActive(num) {
    var dot = document.querySelector("#navigation-dot-".concat(num));
    dot.classList.toggle("active");
  };

  var setImage = function setImage(num) {
    imageSlider.style.background = "url('" + imageSliderModel.images[num] + "') no-repeat center";
  };

  var setAllDotsNotActive = function setAllDotsNotActive() {
    var dots = document.querySelectorAll(".dot");
    console.log({
      dots: dots
    });
    dots.forEach(function (dot) {
      if (dot.classList.contains("active")) {
        console.log(dot);
        dot.classList.toggle("active");
      }
    });
  };

  var createNavigationDots = function createNavigationDots() {
    var i = 0;

    while (i <= numOfImages) {
      var navigationDot = document.createElement("div");
      navigationDot.setAttribute("id", "navigation-dot-".concat(i));

      if (i === 0) {
        navigationDot.classList.toggle("dot");
        navigationDot.classList.toggle("active");
      } else {
        navigationDot.classList.toggle("dot");
      }

      imageSliderContainer.insertBefore(navigationDot, navRight);
      i++;
    }
  };

  var addNavigationDotEventListener = function addNavigationDotEventListener() {
    var dots = document.querySelectorAll(".dot");
    dots.forEach(function (dot) {
      dot.addEventListener("click", function (e) {
        var dotId = e.target.getAttribute("id");
        setAllDotsNotActive();
        sliderCount = dotId[dotId.length - 1];
        setImage(sliderCount);
        setDotActive(sliderCount);
      });
    });
  };

  var addNavigationEventListener = function addNavigationEventListener() {
    var navLeft = document.querySelector("#image-slider-navigation-left");
    var navRight = document.querySelector("#image-slider-navigation-right");
    var navigation = [navLeft, navRight];
    navigation.forEach(function (nav) {
      nav.addEventListener("click", function (e) {
        if (e.target.getAttribute("id") === "image-slider-navigation-left") {
          if (sliderCount >= 1) {
            sliderCount--;
            setImage(sliderCount);
            setAllDotsNotActive();
            setDotActive(sliderCount);
          }
        } else {
          if (sliderCount < imageSliderModel.images.length - 1) {
            sliderCount++;
            setImage(sliderCount);
            setAllDotsNotActive();
            setDotActive(sliderCount);
          }
        }

        console.log(sliderCount);
      });
    });
  };

  var advanceWithTimer = function advanceWithTimer(miliseconds) {
    // let i = 0;
    window.setInterval(function () {
      setImage(sliderCount);
      setAllDotsNotActive();

      if (sliderCount >= numOfImages + 1) {
        setAllDotsNotActive();
        sliderCount = 0;
        setImage(sliderCount);
        setDotActive(sliderCount);
      }

      if (sliderCount !== 0) {
        setAllDotsNotActive();
        setDotActive(sliderCount);
      }

      sliderCount++;
    }, miliseconds);
  };

  return {
    setImage: setImage,
    createNavigationDots: createNavigationDots,
    addNavigationEventListener: addNavigationEventListener,
    addNavigationDotEventListener: addNavigationDotEventListener,
    advanceWithTimer: advanceWithTimer
  };
};



/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _viewController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./viewController */ "./src/viewController.js");
/* harmony import */ var _imageSliderViewController__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./imageSliderViewController */ "./src/imageSliderViewController.js");



var main = function main() {
  var view = Object(_viewController__WEBPACK_IMPORTED_MODULE_0__["viewController"])();

  var addDropDownEvent = function addDropDownEvent() {
    var dropdown = document.querySelectorAll(".dropdown");
    var dropdownData = document.querySelectorAll(".dropdown-data");
    dropdown.forEach(function (element) {
      element.addEventListener("click", function (e) {
        view.updateStyle(e.target.nextElementSibling);
      });
    });
    dropdownData.forEach(function (dropdownElement) {
      dropdownElement.addEventListener("mouseleave", function (e) {
        if (!dropdownElement.classList.contains("visible")) {
          view.updateStyle(e.target);
        }
      });
    });
  };

  return {
    addDropDownEvent: addDropDownEvent
  };
}; //export { main };


var app = main();
app.addDropDownEvent();
var imageSliderView = Object(_imageSliderViewController__WEBPACK_IMPORTED_MODULE_1__["imageSliderViewController"])(); // Initial setup of image slider.

imageSliderView.setImage(0);
imageSliderView.addNavigationEventListener();
imageSliderView.createNavigationDots();
imageSliderView.addNavigationDotEventListener();
imageSliderView.advanceWithTimer(5000);

/***/ }),

/***/ "./src/modelController.js":
/*!********************************!*\
  !*** ./src/modelController.js ***!
  \********************************/
/*! exports provided: modelController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "modelController", function() { return modelController; });
var modelController = function modelController() {
  var checkSiblingElement = function checkSiblingElement(element) {
    if (element.classList.contains("dropdown-data")) {
      return true;
    } else {
      return false;
    }
  };

  return {
    checkSiblingElement: checkSiblingElement
  };
};



/***/ }),

/***/ "./src/viewController.js":
/*!*******************************!*\
  !*** ./src/viewController.js ***!
  \*******************************/
/*! exports provided: viewController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "viewController", function() { return viewController; });
/* harmony import */ var _modelController__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modelController */ "./src/modelController.js");


var viewController = function viewController() {
  var model = Object(_modelController__WEBPACK_IMPORTED_MODULE_0__["modelController"])();

  var updateStyle = function updateStyle(element) {
    if (model.checkSiblingElement(element)) {
      element.classList.toggle("visible");

      if (window.innerWidth > 600) {
        element.style.left = element.previousElementSibling.offsetLeft + "px";
      } else {
        element.style.left = "auto";
      }
    }
  };

  return {
    updateStyle: updateStyle
  };
};



/***/ })

/******/ });
//# sourceMappingURL=main.js.map